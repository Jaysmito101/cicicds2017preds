:: This file sets up the environment
@echo off
setlocal

echo Checking if python is installed...
call :CheckPython

echo Checking if virtual environment is set up...
call :CheckVirtualEnvSetup

echo Activating virtual environment...
call .\venv\Scripts\activate

echo Check if dataset is downloaded...
call :CheckDataset

echo Installing required modules...
python -m pip install -r requirements.txt

exit /b 0

:: Check if python is installed
:CheckPython
python --version >nul 2>&1
if errorlevel 1 (
    echo Python is not installed. Please install python and try again.
    pause
    exit /b 1
)
echo Python is installed.
exit /b 0

:: Check if virtual environment is set up
:CheckVirtualEnvSetup
if not exist .\venv (
    echo Virtual environment is not set up. Setting up...
    python -m venv venv
    if errorlevel 1 (
        echo Failed to set up virtual environment. Please set up virtual environment and try again.
        pause
        exit /b 1
    )
)
echo Virtual environment is set up.
exit /b 0

:: Check if dataset is downloaded
:CheckDataset
if not exist .\dataset\MachineLearningCSV.md5 (
    echo Dataset is not downloaded. Downloading...

    echo Checking Kaggle module...
    call :CheckKaggleModule

    echo Checking kaggle.json file
    if not exist %USERPROFILE%\.kaggle\kaggle.json (
        echo .kaggle file is not found.
        echo Either download dataset manually or place the .kaggle file in %USERPROFILE%\.kaggle\kaggle.json
        pause
        exit /b 1
    )

    echo Downloading dataset...
    kaggle datasets download -d cicdataset/cicids2017
    if errorlevel 1 (
        echo Failed to download dataset. Please download dataset and try again.
        pause
        exit /b 1
    )

    mkdir dataset

    echo Unzipping dataset...
    powershell -command "Expand-Archive -Force '%~dp0cicids2017.zip' '%~dp0dataset'"
    if errorlevel 1 (
        echo Failed to unzip dataset. Please unzip dataset and try again.
        pause
        exit /b 1
    )

    echo Deleting zip file...
    del /f /q .\cicids2017.zip
    if errorlevel 1 (
        echo Failed to delete zip file. Please delete zip file and try again.
    )
)
echo Dataset is downloaded.
exit /b 0


:: Check if kaggle module is installed
:CheckKaggleModule
python -m pip show kaggle >nul 2>&1
if errorlevel 1 (
    echo Kaggle module is not installed. Installing...
    python -m pip install kaggle
    if errorlevel 1 (
        echo Failed to install kaggle module. Please install kaggle module and try again.
        pause
        exit /b 1
    )
)
echo Kaggle module is installed.
exit /b 0